from datetime import date


class Car:
    def __init__(self, brand, color, production_year):
        self.brand = brand
        self.color = color
        self.production_year = production_year
        self.milage = 0
        self.age = self.get_age()

    def drive(self, distance):
        self.milage += distance

    def get_age(self):
        age = date.today().year - self.production_year
        return age

    def repaint(self, new_color):
        self.color = new_color
        return self.color


if __name__ == '__main__':
    car_1 = Car("Fiat", "Grey", 2023)
    print(f"Brand: {car_1.brand}. Color: {car_1.color}. Production year: {car_1.production_year}")
    car_1.drive(1000)
    print("Current milage:", car_1.milage)
    car_1.get_age()
    print("Current age:", car_1.age)
    car_1.repaint("Yellow")
    print("Current color:", car_1.color)
    car2 = Car("Opel", "Blue", 2001)
    car3 = Car("Iveco", "White", 1998)
