import employee_management
from assertpy import assert_that


def test_generate_employee_dictionary_metode_1():
    employee_dictionary = employee_management.generate_employee_dictionary()
    assert "email" in employee_dictionary
    assert "seniority_years" in employee_dictionary
    assert "female" in employee_dictionary


def test_generate_employee_dictionary_metode_2():
    employee_dictionary = employee_management.generate_employee_dictionary()
    assert sorted(["email", "seniority_years", "female"]) == sorted(employee_dictionary.keys())


def test_two_generated_employee_dictionaries_are_different_metode_1():
    employee_1 = employee_management.generate_employee_dictionary()
    employee_2 = employee_management.generate_employee_dictionary()
    assert employee_1 != employee_2


def test_two_generated_employee_dictionaries_are_different_metode_2():
    employee_1 = employee_management.generate_employee_dictionary()
    employee_2 = employee_management.generate_employee_dictionary()
    assert assert_that(employee_1).is_not_equal_to(employee_2)


def test_generating_list_of_employees():
    employees = employee_management.generate_list_of_employees(10)
    assert len(employees) == 10


def test_filtering_list_of_employees_for_seniority():
    input_list = [
        {'email': 'chris@example.com', 'seniority_years': 10, 'female': False},
        {'email': 'alex@example.com', 'seniority_years': 4, 'female': True},
        {'email': 'sam@example.com', 'seniority_years': 17, 'female': True},
        {'email': 'jordan@example.com', 'seniority_years': 30, 'female': True}
    ]
    senior_emails = employee_management.filter_list_of_employees_by_seniority_greater_than(input_list, 10)
    assert senior_emails == ['sam@example.com', 'jordan@example.com']


def test_filtering_list_of_male_only_employees_for_female():
    input_list = [
        {'email': 'chris@example.com', 'seniority_years': 13, 'female': False},
        {'email': 'alex@example.com', 'seniority_years': 4, 'female': False},
        {'email': 'sam@example.com', 'seniority_years': 17, 'female': False},
        {'email': 'jordan@example.com', 'seniority_years': 30, 'female': False}
    ]
    female_employees = employee_management.filter_list_of_employees_by_gender(input_list)
    assert len(female_employees) == 0


def test_filtering_list_of_female_only_employees_for_female():
    input_list = [
        {'email': 'chris@example.com', 'seniority_years': 13, 'female': True},
        {'email': 'alex@example.com', 'seniority_years': 4, 'female': True},
        {'email': 'sam@example.com', 'seniority_years': 17, 'female': True},
        {'email': 'jordan@example.com', 'seniority_years': 30, 'female': True}
    ]
    female_employees = employee_management.filter_list_of_employees_by_gender(input_list)
    assert len(female_employees) == 4


def test_filtering_list_of_employees_for_female():
    input_list = [
        {'email': 'chris@example.com', 'seniority_years': 13, 'female': False},
        {'email': 'alex@example.com', 'seniority_years': 4, 'female': True},
        {'email': 'sam@example.com', 'seniority_years': 17, 'female': True},
        {'email': 'jordan@example.com', 'seniority_years': 30, 'female': True}
    ]
    female_employees = employee_management.filter_list_of_employees_by_gender(input_list)
    assert len(female_employees) == 3
