from classes import Car


def test_car():
    car = Car("Fiat", "Grey", 2023)
    assert car.brand == "Fiat"
    assert car.color == "Grey"
    assert car.production_year == 2023

def test_drive():
    car = Car("Fiat", "Grey", 2023)
    car.drive(100)
    assert car.milage == 100

def test_age():
    car = Car("Fiat", "Grey", 2023)
    assert car.get_age() == 1
def test_repaint():
    car = Car("Fiat", "Grey", 2023)
    car.repaint("Yellow")
    assert car.color == "Yellow"
