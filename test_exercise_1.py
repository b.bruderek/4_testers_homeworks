import pytest

from exercise_1 import check_car_has_warranty


@pytest.mark.parametrize("year, mileage, expected",
                         [(2020, 30000, True), (2020, 70000, False), (2016, 30000, False), (2016, 120000, False)])
def test_car_warranty_status(year, mileage, expected):
    assert check_car_has_warranty(year, mileage) == expected

# def test_car_year_production_2020_and_mileage_30000():
#    assert check_car_has_warranty(2020, 30000) == True


# def test_car_year_production_2020_and_mileage_70000():
#    assert check_car_has_warranty(2020, 70000) == False


# def test_car_year_production_2016_and_mileage_30000():
#    assert check_car_has_warranty(2016, 30000) == False


# def test_car_year_production_2016_and_mileage_120000():
#    assert check_car_has_warranty(2016, 120000) == False
