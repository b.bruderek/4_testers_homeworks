import random
from datetime import date

female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']


def get_firstname(is_female):
    if is_female == True:
        return random.choice(female_fnames)
    else:
        return random.choice(male_fnames)


def get_surname():
    random_surname = random.choice(surnames)
    return random_surname


def get_country():
    random_country = random.choice(countries)
    return random_country


def generate_age():
    random_age = random.randint(5, 45)
    return random_age


def is_person_an_adult(age):
    if age >= 18:
        return True
    else:
        return False


def generate_person_dictionary(is_female):
    firstname = get_firstname(is_female)
    surname = get_surname()
    email = f"{firstname.lower()}.{surname.lower()}@excample.com"
    country = get_country()
    age = generate_age()
    adult = is_person_an_adult(age)
    birth_year = date.today().year - age
    return {
        "firstname": firstname,
        "lastname": surname,
        "country": country,
        "email": email,
        "age": age,
        "adult": adult,
        "birth_year": birth_year
    }


def generate_persons_list():
    person_list = []
    for i in range(10):
        if i % 2 == 0:
            person_list.append(generate_person_dictionary(True))
        else:
            person_list.append(generate_person_dictionary(False))
    return person_list


def print_descriptions(persons):
    for person in persons:
        print(
            f"Hi! I'm {person["firstname"]} {person["lastname"]}. I come from {person["country"]} and I was born in {person["birth_year"]}")


if __name__ == '__main__':
    print(generate_persons_list())
    persons = generate_persons_list()
    print_descriptions(persons)
