from datetime import date


def check_car_has_warranty(car_production_year, car_mileage_in_kilometers):
    current_year = date.today().year
    vehicle_age = current_year - car_production_year
    if vehicle_age <= 5 and car_mileage_in_kilometers <= 60_000:
        return True
    else:
        return False


if __name__ == '__main__':
    print(check_car_has_warranty(2020, 30000))
    print(check_car_has_warranty(2020, 70000))
    print(check_car_has_warranty(2016, 30000))
    print(check_car_has_warranty(2016, 120000))
