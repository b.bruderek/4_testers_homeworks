import random


# part_1
def generate_email_adress():
    names_list = ["Sam", "Chris", "Alex", "Jordan", "Taylor", "Gale", "Lee"]
    name = random.choice(names_list)
    email_adress = name.lower() + "@example.com"
    return email_adress


def generate_seniority_years():
    seniority_years = random.randrange(41)
    return seniority_years


def generate_gender():
#    is_female_list = [True, False]
#    is_female = random.choice(is_female_list)
    is_female = bool(random.randint(0,1))
    return is_female


def generate_employee_dictionary():
    email_adress = generate_email_adress()
    seniority_years = generate_seniority_years()
    is_female = generate_gender()
    return {
        "email": email_adress,
        "seniority_years": seniority_years,
        "female": is_female
    }


# part_2

def generate_list_of_employees(list_length):
    employees = [generate_employee_dictionary() for i in range(list_length)]
    return employees


# part_3

def filter_list_of_employees_by_seniority_greater_than(employees, seniority_years):
    return [employee["email"] for employee in employees  if employee["seniority_years"] > seniority_years]


def filter_list_of_employees_by_gender(employees, female=True):
    return [employee for employee in employees if employee["female"] == female]


if __name__ == '__main__':
    print("----- Part 1 -----")
    print(generate_employee_dictionary())
    print(generate_employee_dictionary())
    print(generate_employee_dictionary())
    print("----- part 2 -----")
    employees = generate_list_of_employees(10)
    print(employees)
    print("----- part 3 -----")
    senior_employees = filter_list_of_employees_by_seniority_greater_than(employees,10)
    print(senior_employees)
    female_employees = filter_list_of_employees_by_gender(employees)
    print(female_employees)
